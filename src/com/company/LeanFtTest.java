package com.company;

import com.hp.lft.report.*;
import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;

import unittesting.*;

public class LeanFtTest extends UnitTestClassBase {

    protected Browser browser;
    protected TestingGround app;

    @BeforeClass
    public void beforeClass() throws Exception {
        browser = BrowserFactory.launch(BrowserType.CHROME);
    }

    @AfterClass
    public void afterClass() throws Exception {
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
    }

    @AfterMethod
    public void afterMethod() throws Exception {
    }

    @Test
    public void test() throws GeneralLeanFtException {
        this.app = new TestingGround(browser);

        browser.navigate("http://testing-ground.scraping.pro/login");
        app.webScraperTestingGroundPage().userNameEditField().setValue("admin");
        app.webScraperTestingGroundPage().pwdEditField().setValue("asd");
        app.webScraperTestingGroundPage().loginButton().click();

        String textFound = "";
        if(app.webScraperTestingGroundPage().loginSuccessMessage().exists()){
            textFound = app.webScraperTestingGroundPage().loginSuccessMessage().getInnerText();
        }

        String textExpected = "WELCOME :)";

        VerificationData verificationData = new VerificationData();
        verificationData.name = "loginStatus";
        verificationData.description = "Verify if login is sucessfull";
        verificationData.operationName = "Verify login";

        VerificationParameter expected = new VerificationParameter("text expected", textExpected);
        VerificationParameter found = new VerificationParameter("text found", textFound);

        verificationData.verificationParameters.add(expected);
        verificationData.verificationParameters.add(found);

        try{
            if(textFound.equalsIgnoreCase(textExpected)){
                Reporter.reportVerification(Status.Passed, verificationData);
            }
            else{
                Reporter.reportVerification(Status.Failed, verificationData);
                Assert.assertFalse(true, "Test failed. See LeanFT report for more details");
            }
        }
        catch(ReportException e){
            e.printStackTrace();
        }

    }

}